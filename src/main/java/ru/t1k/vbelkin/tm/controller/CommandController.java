package ru.t1k.vbelkin.tm.controller;

import ru.t1k.vbelkin.tm.api.controller.ICommandController;
import ru.t1k.vbelkin.tm.api.service.ICommandService;
import ru.t1k.vbelkin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1k.vbelkin.tm.exception.system.CommandNotSupportedException;
import ru.t1k.vbelkin.tm.model.Command;
import ru.t1k.vbelkin.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("usageMemory (bytes): " + usageMemoryFormat);
    }

    @Override
    public void showVersion() {
        System.out.println("[Version]");
        System.out.println("1.5.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[About]");
        System.out.println("Name: Vadim Belkin");
        System.out.println("E-mail: vbelkin@tsconsulting.ru");
        System.out.println("Web-Site: https://gitlab.com/VBELKIN");
    }

    @Override
    public void showHelp() {
        System.out.println("[Help]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
            System.out.println(name);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
            System.out.println(argument);
        }
    }
}
