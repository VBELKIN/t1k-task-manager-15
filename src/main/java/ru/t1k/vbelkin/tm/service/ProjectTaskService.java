package ru.t1k.vbelkin.tm.service;

import ru.t1k.vbelkin.tm.api.repository.IProjectRepository;
import ru.t1k.vbelkin.tm.api.repository.ITaskRepository;
import ru.t1k.vbelkin.tm.api.service.IProjectTaskService;
import ru.t1k.vbelkin.tm.exception.entity.TaskNotFoundException;
import ru.t1k.vbelkin.tm.exception.field.ProjectIdEmptyException;
import ru.t1k.vbelkin.tm.exception.field.TaskIdEmptyException;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
