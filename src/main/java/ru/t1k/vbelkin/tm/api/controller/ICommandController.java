package ru.t1k.vbelkin.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showVersion();

    void showAbout();

    void showHelp();

    void showArguments();

    void showCommands();

}
