package ru.t1k.vbelkin.tm.api.controller;

import ru.t1k.vbelkin.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();
}
